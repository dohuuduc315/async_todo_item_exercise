﻿using Async_TODO_item_exercise;
using Newtonsoft.Json;


var client = new HttpClient();  //create instance of HttpClient
var request = await client.GetAsync("https://jsonplaceholder.typicode.com/todos");  //make an asynchronous HTTP GET request to the API endpoint
var response = await request.Content.ReadAsStringAsync();   //read the response content as a string
var todoList = JsonConvert.DeserializeObject<List<Todo>>(response); //deserialize the JSON response into a list of TODO items
foreach (var item in todoList)
{
    Console.WriteLine($"userId: {item.userId}, id: {item.id}, title: {item.title}, completed: {item.completed}");
}